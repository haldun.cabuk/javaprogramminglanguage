package day_018_hakan;

public class Task_69 {

    public static void main(String[] args) {
        System.out.println(sayiAsalMi_2(5)); // true
        System.out.println(sayiAsalMi_2(0)); // false
        System.out.println(sayiAsalMi_2(1)); // false
        System.out.println(sayiAsalMi_2(2)); // true
        System.out.println(sayiAsalMi_2(17)); // true
        System.out.println(sayiAsalMi_2(42)); // false
    }

    public static boolean sayiAsalMi(int num){
        if(num == 2){   // 2 asal sayi oldugu icin, true dönecek
            return true;
        } else if(2 < num){ // 2 den büyükse...

            if(num%2 == 0){ // cift sayi ise false dön
                return false;
            } else { // tek sayi ve 2 den büyük ise

                for(int i =3; i < num; i++){ // 3'ten, kendisinden 1 eksige kadar sayilara tam bölünüp bölünmedigini kontrol ediyoruz
                    if(num%i == 0){ // tam böleni varsa false dön
                        return false;
                    }
                }

            }
        } else { // 0,1 ise false dön
            return false;
        }

        return true;
    }

    public static boolean sayiAsalMi_2(int num){

        if(num < 2){
            return false;
        }

        for(int i=2; i < num; i++){ // 2 degeri ozel bir deger
            if(num%i == 0){
                return false;
            }
        }

        return true;
    }
}
