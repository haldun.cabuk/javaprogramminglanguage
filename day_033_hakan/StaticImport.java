package day_033_hakan;

import java.util.ArrayList;

import static java.lang.Math.*;


public class StaticImport {

    /**
     * static import, bize class ismini kullanmadan,
     * o class in icerisindeki method isimlerini yazarak erisebilmemize olanak saglar.
     *
     * Fakat, class ismini kullanmak, o methodun nereye ait oldugunu görmek ve kodu daha kolay okuyabilmek icin tercih edilmesi tavsiye edilir.
     */

    public static void main(String[] args) {
        System.out.println(min(5, 6)); // 5
        System.out.println(max(5, 6)); // 6

        System.out.println(pow(2,3));
        ArrayList<String> list = new ArrayList();
    }
}
