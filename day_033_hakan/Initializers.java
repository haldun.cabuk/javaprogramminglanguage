package day_033_hakan;

import java.util.ArrayList;

public class Initializers {

    // STATIC INITIALIZER : Class loading
    static int a = 5;

    static {
        System.out.println("static Initializer 1 " + a );
    }

    public Initializers(){
        System.out.println("Constructor triggered");
    }

    static int b = 10;

    // Object Initializer
    {
        System.out.println("Object Initializer 1 ");
    }

    static {
        System.out.println("static Initializer 2 " + b + a);
    }

    // Object Initializer
    {
        System.out.println("Object Initializer 2 ");
    }

    static {
        System.out.println("static Initializer 3 ");
    }



    public static void main(String[] args) {

    }


}
